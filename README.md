# Object Dataset Tools

The tools to create object masks, bounding box labels, and 3D reconstructed object mesh (.ply) for object sequences filmed with an RGB-D camera. This project can prepare training and testing data for various deep learning projects such as 6D object pose estimation projects singleshotpose, and many object detection (e.g., faster rcnn) and instance segmentation (e.g., mask rcnn) projects.

## Docker Image
```
wangep1/6d_pose_estimation:object_dataset_tools_0509
```

## Installation
- Anaconda3
```
wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
sudo bash Anaconda3-2020.11-Linux-x86_64.sh
vim ~/.bashrc
export PATH=$PATH:/root/anaconda3/bin
source ~/.bashrc
conda info --envs
```
- Build conda environment
```
conda create -n ObjectDatasetTools python=2.7
# To activate this environment, use
#     $ conda activate ObjectDatasetTools
# To deactivate an active environment, use
#     $ conda deactivate
```
- requirements
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential cmake git pkg-config libssl-dev libgl1-mesa-glx
conda activate ObjectDatasetTools
pip install numpy Cython==0.19 pypng scipy scikit-learn open3d==0.9.0 scikit-image tqdm pykdtree opencv-python==3.3.0.10 opencv-contrib-python==3.3.0.10  trimesh==2.38.24
```

## Create Dataset
* **ObjectDatasetTools**
	* **LINEMOD**
		* **depth**
		* **JPEGImages**
		* **intrinsics.json**

## Obtain frame transforms
Compute transforms for frames at the specified interval (interval can be changed in config/registrationParameters) against the first frame, save the transforms(4*4 homogenous transforms) as a numpy array (.npy).
```
python compute_gt_poses.py LINEMOD/sugar
```

## Register all frames and create a mesh for the registered scene.
```
python register_scene.py LINEMOD/sugar
python register_segmented.py LINEMOD/sugar (not very good)
```

## Process the registered pointcloud manually

## Create image masks and label files
```
python create_label_files.py LINEMOD/sugar
python inspectMasks.py LINEMOD/sugar
```
## Train and test images
```
python makeTrainTestfiles.py
```

## Get the object scale(max vertice distance)
```
python getmeshscale.py
```

## Create bounding box labels for object detection projects
```
python get_BBs.py
```
